"""
Testbench mos_id_tb
===============

"""

from sal.testbench_base import TestbenchBase
from .params import mos_id_tb_params


class testbench(TestbenchBase):
    def __init__(self):
        super().__init__()
        self.params = None

    @property
    def package(self):
        return "mos_id_tb"

    @classmethod
    def parameter_class(cls):
        """Return the parameter class"""
        return mos_id_tb_params

    # Define template draw and schematic parameters below using property decorators:
    @property
    def params(self) -> mos_id_tb_params:
        return self._params

    @params.setter
    def params(self, val: mos_id_tb_params):
        self._params = val
