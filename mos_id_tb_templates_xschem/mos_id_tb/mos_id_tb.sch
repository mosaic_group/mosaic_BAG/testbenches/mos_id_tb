v {xschem version=3.4.5 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
C {devices/vsource.sym} 670 -520 0 0 {name=VS value=\{vs\}}
C {devices/isource.sym} 670 -360 0 0 {name=IBIAS value=\{ibias\}}
C {devices/gnd.sym} 670 -490 0 0 {name=l1 lab=GND}
C {devices/vsource.sym} 670 -200 0 0 {name=VBIAS value=1}
C {devices/lab_pin.sym} 870 -490 3 0 {name=l7 sig_type=std_logic lab=vgs}
C {devices/lab_pin.sym} 770 -650 3 0 {name=l2 sig_type=std_logic lab=vs}
C {devices/lab_pin.sym} 840 -700 2 0 {name=l3 sig_type=std_logic lab=vs}
C {devices/lab_pin.sym} 700 -700 0 0 {name=l4 sig_type=std_logic lab=vgs}
C {devices/lab_pin.sym} 670 -550 1 0 {name=l6 sig_type=std_logic lab=vs}
C {devices/lab_pin.sym} 670 -390 1 0 {name=l8 sig_type=std_logic lab=ibias}
C {devices/lab_pin.sym} 670 -330 3 0 {name=l9 sig_type=std_logic lab=vs}
C {devices/vsource.sym} 770 -520 0 0 {name=VGS value=\{vgs\}}
C {devices/lab_pin.sym} 770 -550 1 0 {name=l11 sig_type=std_logic lab=vgs}
C {devices/lab_pin.sym} 770 -490 3 0 {name=l10 sig_type=std_logic lab=vs}
C {devices/lab_pin.sym} 670 -170 3 0 {name=l16 sig_type=std_logic lab=vs}
C {devices/lab_pin.sym} 670 -230 1 0 {name=l17 sig_type=std_logic lab=ibias}
C {devices/code_shown.sym} 40 -340 0 0 {name=SPICE_MAIN
only_toplevel=false 

value=".save all

.control

let vgs_step = ($&vgs_stop - $&vgs_start) / $&vgs_num
dc vgs $&vgs_start $&vgs_stop $&vgs_step
setscale vgs

write result.raw dc.all

.endc"
}
C {devices/code_shown.sym} 40 -750 0 0 {name=SPICE_PARAMS 
only_toplevel=false 
value="
* NOTE: auto-generated code block,
*       created by gen sim during 
*       concrete testbench instantiation

.lib sky130.lib.spice tt
.param mc_mm_switch=0

.param vs=1nF
.param vgs=1.8
.param ibias=0
.param vgs_start=0
.param vgs_stop=1.8
.param vgs_num=200

"}
C {mos_analogbase_gen/mos_analogbase_templates_xschem/mos_analogbase/mos_analogbase.sym} 680 -640 0 0 {name=XDUT
spiceprefix=X
}
C {devices/lab_pin.sym} 770 -750 1 0 {name=l5 sig_type=std_logic lab=vd}
C {devices/lab_pin.sym} 870 -550 1 0 {name=l12 sig_type=std_logic lab=vd}
C {devices/vsource.sym} 870 -520 0 0 {name=VD value=0}
