#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import ParamsBase
from sal.simulation.simulation_output import *
from sal.testbench_params import *


@dataclass
class mos_id_tb_params(TestbenchParamsBase):
    """
    Parameter class for mos_id_tb

    Args:
    ----

    dut: DUT
        inherited from TestbenchParamsBase, encapsulates the DUT lib and cell

    sch_params: TestbenchSchematicParams
        inherited from TestbenchParamsBase, encapsulates schematic related parameters

    vgs: float

    vs: float

    vgs_start: float

    vgs_stop: float

    vgs_num: int

    """

    vgs: float
    vs: float

    vgs_start: float
    vgs_stop: float
    vgs_num: int

    @classmethod
    def builtin_outputs(cls) -> Dict[str, SimulationOutputBase]:
        """
        Builtin outputs are merged into the effective simulation params
        """
        return {
            'vgs': SimulationOutputVoltage(analysis='dc', signal='vgs', quantity=VoltageQuantity.V),
            'ibias': SimulationOutputCurrent(analysis='dc', signal='vd', terminal='/VGS/MINUS'),
        }

    @classmethod
    def defaults(cls) -> mos_id_tb_params:
        return mos_id_tb_params(
            dut=DUT.placeholder(),  # NOTE: Framework will inject DUT
            dut_wrapper_params=None,  # NOTE: generator can decide to use a wrapper
            sch_params=TestbenchSchematicParams(
                dut_conns=[],
                v_sources=[],
                i_sources=[],
                instance_cdf_parameters={}
            ),
            simulation_params=TestbenchSimulationParams(
                variables={},
                sweeps={},
                outputs={}
            ),
            vgs=0,
            vs=0,
            vgs_start=0,
            vgs_stop=1.0,
            vgs_num=200,
        )
